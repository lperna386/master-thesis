#ifndef MY_MESSENGER_HH
#define MY_MESSENGER_HH
#include "G4UImessenger.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIdirectory.hh"

#include "SteppingAction.hh"


class MyMessenger : public G4UImessenger {
public:
    MyMessenger(SteppingAction* sa);
    ~MyMessenger();

    void SetNewValue(G4UIcommand* command, G4String newValue);

private:
    SteppingAction* mySteppingAction;
    G4UIdirectory* myDir;
    G4UIcmdWithADouble* myFloatCmd;
};

#endif