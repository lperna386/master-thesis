// Standard include and ifdef
#include <G4MTRunManager.hh>
using RunManager = G4MTRunManager;

#include <G4VisExecutive.hh>
#include <G4UIExecutive.hh>

#include <G4String.hh>
#include <G4UImanager.hh>
#include <vector>

// Import the G4 Physics Lists.
#include <QGSP_BERT.hh>  // This is used in random orientation
#include "FTFP_BERT.hh"  // This is used in axial alignment, with modifications
#include "EmStandardPhysics.hh"  // Necessary to modify the PhysicsList

// Some other imports
#include "ActionInitialization.hh"
#include "DetectorConstruction.hh"
#include "PhysicsList.hh"

// Adding something
#include "G4RunManagerFactory.hh"
#include "G4SteppingVerbose.hh"
#include "G4FastSimulationPhysics.hh"
#include "Randomize.hh"

using namespace std;

int main(int argc, char** argv)
{
    // Begin printout in a somewhat verbose mode
    cout << "-----" << endl;
    cout << "| main.cc: let's start!" << endl;
    cout << "-----" << endl;

    // Detect interactive mode (if no arguments) and define UI session
    G4UIExecutive* ui = nullptr;
    if ( argc == 1 ) { ui = new G4UIExecutive(argc, argv); }

    // Use G4SteppingVerboseWithUnits
    G4int precision = 4;
    G4SteppingVerbose::UseBestUnit(precision);

    // Before invoking the RunManager, it is necessary to set the random seed
    // generator and the random seed state. MTRunManager will then clone these
    // properties in each worker called in multi-threading mode.
    G4Random::setTheSeed((unsigned)clock());

    // Load the run manager and set the verbosity and number of threads
    auto runManager = new RunManager();
    G4int NumOfG4Threads = 4;
    runManager->SetNumberOfThreads(NumOfG4Threads);
    runManager->SetVerboseLevel(0);  // <<< set run manager verbosity here

    // Set mandatory initialization classes: detector construction 
    DetectorConstruction* modifiedDetectorConstruction = new DetectorConstruction();

    // Set the Physics List: FTFP_BERT in "amorphous" mode (bChanneling=false;)
    // or a custom one in "axial" mode (bChanneling=true;)
	G4bool bChanneling = false;
    modifiedDetectorConstruction->SetChanneling(bChanneling);
    if(bChanneling){
        //     "matID = 2" for PWO <001> axis in axial
        //     "matID = 3" for W <111> axis in axial (electrons)
        //     "matID = 4" for W <111> axis in axial (positrons)
        //     "matID = 5" for W <111> axis at 3 mrad (electrons)
        //     "matID = 6" for W <111> axis at 3 mrad (positrons)
        G4int matID = 2;   // PWO <001> axial
        G4String nullstr = "";
        G4int PhysicsListVerbosity = 1;   // 0: No verbosity; 1: some info...

        // Load a standard Physics List and replace the electromagnetic part
        G4VModularPhysicsList* modifiedPhysicsList = new FTFP_BERT(PhysicsListVerbosity);
        modifiedPhysicsList->ReplacePhysics(new EmStandardPhysics(PhysicsListVerbosity,nullstr,matID));
        runManager->SetUserInitialization(modifiedPhysicsList);
    }else{
        // Option: "random" mode.
        G4int PhysicsListVerbosity = 1;   // 0: No verbosity; 1: some info...
        runManager->SetUserInitialization(new FTFP_BERT(PhysicsListVerbosity));
        // runManager->SetUserInitialization(new QGSP_BERT(PhysicsListVerbosity)); // Here for historical reasons...
    }
    
    // Set the custom classes (RunAction, EventAction, DetectorConstruction, etc.)
    runManager->SetUserInitialization(modifiedDetectorConstruction);
    runManager->SetUserInitialization(new ActionInitialization());

    // Initialize visualization
    G4VisManager* visManager = new G4VisExecutive;
    visManager->Initialize();

    // Get the pointer to the User Interface manager
    G4UImanager* UImanager = G4UImanager::GetUIpointer();

    // Process macro or start UI session
    if ( ! ui ) {
        // batch mode
        G4String command = "/control/execute ";
        G4String fileName = argv[1];
        UImanager->ApplyCommand(command+fileName);
    }
    else {
        // interactive mode
        UImanager->ApplyCommand("/control/execute init_vis.mac");
        ui->SessionStart();
        delete ui;
    }

    // Job termination: free the store. Note: user actions, physics_list and 
    // detector_description are owned and deleted by the run manager, so 
    // they should not be deleted in the main() program !
    delete visManager;
    delete runManager;

    // END
    cout << "-----" << endl;
    cout << "| main.cc: done, goodbye!" << endl;
    cout << "-----" << endl;

    return 0;
}
