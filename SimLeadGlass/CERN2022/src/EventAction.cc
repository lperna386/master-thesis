#include <G4SystemOfUnits.hh>
#include <G4SDManager.hh>
#include <G4THitsMap.hh>
#include <G4Event.hh>

#include "CustomHit.hh"
#include "EventAction.hh"
#include "RunAction.hh"

#include "G4AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"

using namespace std;

// EventAction::EndOfEventAction, executed at the end of each event
void EventAction::EndOfEventAction(const G4Event* event)
{
    // Load the sensitive detector manager (set verbosity in DetectorConstruction.cc)
    G4SDManager* sdm = G4SDManager::GetSDMpointer();
	
    // Load the analysis manager for data output (set verbosity in RunAction.cc)
    G4AnalysisManager* analysis = G4AnalysisManager::Instance();

    // Get the set of all the data collections for the current event. If the set is
    // empty, then exit the function.
    G4HCofThisEvent* hcofEvent = event->GetHCofThisEvent();
    if(!hcofEvent) return;
	
    // If there are hits, process them. Specifically, start by getting each collection separately
    // Note to self: the syntax for the argument of GetCollectionID is always "NameofSD/NameofFunction"
    G4int fPhCalEDepId_CC = sdm->GetCollectionID("PhCalTest_CC_SD/VolumeEDep");  // photon calorimeter

    // leo getcollid per TopLeft calo 
    G4int fPhCalEDepId_TL = sdm->GetCollectionID("PhCalTest_TL_SD/VolumeEDep");  // photon calorimeter

    // leo getcollid per TopRight calo 
    G4int fPhCalEDepId_TR = sdm->GetCollectionID("PhCalTest_TR_SD/VolumeEDep");  // photon calorimeter

    // leo getcollid per BottomLeft calo 
    G4int fPhCalEDepId_BL = sdm->GetCollectionID("PhCalTest_BL_SD/VolumeEDep");  // photon calorimeter

    // leo getcollid per BottomRight calo 
    G4int fPhCalEDepId_BR = sdm->GetCollectionID("PhCalTest_BR_SD/VolumeEDep");  // photon calorimeter

    // leo getcollid per CenterLeft calo 
    G4int fPhCalEDepId_CL = sdm->GetCollectionID("PhCalTest_CL_SD/VolumeEDep");  // photon calorimeter

    // leo getcollid per CenterRight calo 
    G4int fPhCalEDepId_CR = sdm->GetCollectionID("PhCalTest_CR_SD/VolumeEDep");  // photon calorimeter
    
    G4int fTgtIdA = sdm->GetCollectionID("TgtA_SD/VolumeEDep");  // crystal
    G4int fTgtIdB = sdm->GetCollectionID("TgtB_SD/VolumeEDep");  // crystal
    G4int fTgtIdC = sdm->GetCollectionID("TgtC_SD/VolumeEDep");  // crystal
    
    
    G4int fTrackerId0 = sdm->GetCollectionID("Tracker_SD_0/VolumeTracking");  // tracking module 0
    G4int fTrackerId1 = sdm->GetCollectionID("Tracker_SD_1/VolumeTracking");  // tracking module 1
    
    // Cast the outputs in a more appropriate form
    VolumeEDepHitsCollection* hitCollectionPhCal_CC = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_CC));

    // leo idem per TopLeft calorimetro 
    VolumeEDepHitsCollection* hitCollectionPhCal_TL = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_TL));

    // leo idem per TopRight calorimetro 
    VolumeEDepHitsCollection* hitCollectionPhCal_TR = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_TR));
    
    // leo idem per BottomLeft calorimetro 
    VolumeEDepHitsCollection* hitCollectionPhCal_BL = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_BL));

    // leo idem per BottomRight calorimetro 
    VolumeEDepHitsCollection* hitCollectionPhCal_BR = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_BR));

    // leo idem per CenterLeft calorimetro 
    VolumeEDepHitsCollection* hitCollectionPhCal_CL = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_CL));

    // leo idem per CenterRight calorimetro 
    VolumeEDepHitsCollection* hitCollectionPhCal_CR = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fPhCalEDepId_CR));

    VolumeEDepHitsCollection* hitCollectionTgtA = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fTgtIdA));
    VolumeEDepHitsCollection* hitCollectionTgtB = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fTgtIdB));
    VolumeEDepHitsCollection* hitCollectionTgtC = dynamic_cast<VolumeEDepHitsCollection*>(hcofEvent->GetHC(fTgtIdC));
    
    VolumeTrackingHitsCollection* hitCollectionTracker0 = dynamic_cast<VolumeTrackingHitsCollection*>(hcofEvent->GetHC(fTrackerId0));
    VolumeTrackingHitsCollection* hitCollectionTracker1 = dynamic_cast<VolumeTrackingHitsCollection*>(hcofEvent->GetHC(fTrackerId1));

    {analysis->FillNtupleDColumn(0, 0, event->GetEventID());}
	
    // ============================================================================
    //                               SILICON TRACKERS
    // ============================================================================

    // get tracking system data collection
    // 1 hit per particle, per step & per tracking plane
    // --> consider only hits whose energy deposit is over threshold
    // --> for hits belonging to the same particle, compute mean between all steps transverse positions
    // --> different particles are treated separately (and increase the hit counter)
    G4double thresholdTrackerEDep = 50 * keV;
	
    // --> module 0 (Telescope n. 1, single plane)
    if (hitCollectionTracker0)
    {
        G4int lastTrackId0 = -1;
        G4int NStep0 = 1;
        G4int NHits0 = 0;
        G4double horsa0 = -9999.0*cm;
        G4double versa0 = -9999.0*cm;
        for (auto hit: *hitCollectionTracker0->GetVector())
        {
            // The iteration is over all the registered hits (several hits * several particles)
            if (hit->GetEDep()>thresholdTrackerEDep)
            {
                // If the energy loss of the hit is beyond threshold, add the primary particle 
                // to the number of particles which crossed the active volume (do it only 1 time)
                // Moreover, add the hit coordinates in x and y
                if(hit->GetTrackId() != lastTrackId0)
                {
                    NHits0+=1;
                    NStep0=1;
                    horsa0=hit->GetX()[0];
                    versa0=hit->GetX()[1];
                }
                else
                {
                    NStep0+=1;
                    horsa0+=hit->GetX()[0];
                    versa0+=hit->GetX()[1];
                }
                lastTrackId0 = hit->GetTrackId();
            }
        }
        // The position measured in this event will be the average value of the positions
        // recorded for each particle with an energy loss beyond threshold.
        horsa0 = horsa0 / NStep0;
        versa0 = versa0 / NStep0;
        analysis->FillNtupleDColumn(0, 1, NHits0);
        analysis->FillNtupleDColumn(0, 2, NHits0);  // columns 1 & 2 are identical -- same silicon layer
        analysis->FillNtupleDColumn(0, 5, horsa0 / cm);
        analysis->FillNtupleDColumn(0, 6, versa0 / cm);
    }
    else
    {
        // Event is crap!
        analysis->FillNtupleDColumn(0, 1, 0);
        analysis->FillNtupleDColumn(0, 2, 0);
        analysis->FillNtupleDColumn(0, 5, -9999.0 / cm);
        analysis->FillNtupleDColumn(0, 6, -9999.0 / cm);
    }
	
    // --> module 1 (Telescope n. 2, single plane)
    // (See module 0 for comments on the code)
    if (hitCollectionTracker1)
    {
        G4int lastTrackId1 = -1;
        G4int NStep1 = 1;
        G4int NHits1 = 0;
        G4double horsa1 = -9999.0*cm;
        G4double versa1 = -9999.0*cm;
        for (auto hit: *hitCollectionTracker1->GetVector())
        {
            if (hit->GetEDep()>thresholdTrackerEDep)
            {
                if(hit->GetTrackId() != lastTrackId1)
                    {
                    NHits1+=1;
                    NStep1=1;
                    horsa1=hit->GetX()[0];
                    versa1=hit->GetX()[1];
                }
                else
                {
                    NStep1+=1;
                    horsa1+=hit->GetX()[0];
                    versa1+=hit->GetX()[1];
                }
                lastTrackId1 = hit->GetTrackId();
            }
        }
        horsa1 = horsa1 / NStep1;
        versa1 = versa1 / NStep1;
        analysis->FillNtupleDColumn(0, 3, NHits1);
        analysis->FillNtupleDColumn(0, 4, NHits1);  // columns 3 & 4 are identical -- same silicon layer
        analysis->FillNtupleDColumn(0, 7, horsa1 / cm);
        analysis->FillNtupleDColumn(0, 8, versa1 / cm);
    }
    else
    {
        analysis->FillNtupleDColumn(0, 3, 0);
        analysis->FillNtupleDColumn(0, 4, 0);
        analysis->FillNtupleDColumn(0, 7, -9999.0 / cm);
        analysis->FillNtupleDColumn(0, 8, -9999.0 / cm);
    }
	
 
    // ============================================================================
    //                           CALORIMETERS AND SCINTILLATORS
    // ============================================================================
	
    // get photon calorimeter data collection
    // 1 hit per particle & per step --> sum everything for the current event
	
    if (hitCollectionPhCal_CC)
    {
        G4double PhCalEDepTot_CC = 0.0;
        for (auto hit: *hitCollectionPhCal_CC->GetVector())
        {PhCalEDepTot_CC += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, 9, PhCalEDepTot_CC / GeV);
        // leo analysis->FillH1(analysis->GetH2Id("EdepCC"), PhCalEDepTot_CC);
        
    }
    else
    {analysis->FillNtupleDColumn(0, 9, 0.0);}

    // leo inizio TopLeft
    if (hitCollectionPhCal_TL)
    {
        G4double PhCalEDepTot_TL = 0.0;
        for (auto hit: *hitCollectionPhCal_TL->GetVector())
        {PhCalEDepTot_TL += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, 10, PhCalEDepTot_TL / GeV);
        // leo analysis->FillH1(analysis->GetH2Id("EdepTL"), PhCalEDepTot_TL);
    }
    else
    {analysis->FillNtupleDColumn(0, 10, 0.0);}
    // leo fine

    // leo inizio TopRight
    if (hitCollectionPhCal_TR)
    {
        G4double PhCalEDepTot_TR = 0.0;
        for (auto hit: *hitCollectionPhCal_TR->GetVector())
        {PhCalEDepTot_TR += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, 11, PhCalEDepTot_TR / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, 11, 0.0);}
    // leo fine
   

    // leo inizio BottomLeft
    if (hitCollectionPhCal_BL)
    {
        G4double PhCalEDepTot_BL = 0.0;
        for (auto hit: *hitCollectionPhCal_BL->GetVector())
        {PhCalEDepTot_BL += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, 12, PhCalEDepTot_BL / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, 12, 0.0);}
    // leo fine

    // leo inizio BottomRight
    if (hitCollectionPhCal_BR)
    {
        G4double PhCalEDepTot_BR = 0.0;
        for (auto hit: *hitCollectionPhCal_BR->GetVector())
        {PhCalEDepTot_BR += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, 13, PhCalEDepTot_BR / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, 13, 0.0);}
    // leo fine

    // leo inizio CenterLeft
    if (hitCollectionPhCal_CL)
    {
        G4double PhCalEDepTot_CL = 0.0;
        for (auto hit: *hitCollectionPhCal_CL->GetVector())
        {PhCalEDepTot_CL += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, 14, PhCalEDepTot_CL / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, 14, 0.0);}
    // leo fine

    // leo inizio CenterRight
    if (hitCollectionPhCal_CR)
    {
        G4double PhCalEDepTot_CR = 0.0;
        for (auto hit: *hitCollectionPhCal_CR->GetVector())
        {PhCalEDepTot_CR += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, 15, PhCalEDepTot_CR / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, 15, 0.0);}
    // leo fine
    
    // get crystal data collection
    // 1 hit per particle & per step --> sum everything for the current event

    if (hitCollectionTgtA)
    {
        G4double PhTgtATot= 0.0;
        for (auto hit: *hitCollectionTgtA->GetVector())
        {PhTgtATot += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, 16, PhTgtATot / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, 16, 0.0);}
    
    if (hitCollectionTgtB)
    {
        G4double PhTgtBTot= 0.0;
        for (auto hit: *hitCollectionTgtB->GetVector())
        {PhTgtBTot += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, 17, PhTgtBTot / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, 17, 0.0);}

    
    if (hitCollectionTgtC)
    {
        G4double PhTgtCTot= 0.0;
        for (auto hit: *hitCollectionTgtC->GetVector())
        {PhTgtCTot += hit->GetEDep();}
        analysis->FillNtupleDColumn(0, 18, PhTgtCTot / GeV);
    }
    else
    {analysis->FillNtupleDColumn(0, 18, 0.0);}


    // Add the line for the new event to ntuple
    analysis->AddNtupleRow(0);
}
