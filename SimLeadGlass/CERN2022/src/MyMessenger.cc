#include "SteppingAction.hh"
#include "MyMessenger.hh"
#include <sstream>

MyMessenger::MyMessenger(SteppingAction* sa, RunAction* ra)
    : G4UImessenger(), mySteppingAction(sa), myRunAction(ra)
{
    myDir = new G4UIdirectory("/myCommand/");
    myDir->SetGuidance("Custom commands for MySteppingAction");

    myFloatCmd = new G4UIcmdWithADouble("/myCommand/zProfile", this);
    myFloatCmd->SetGuidance("Set the float number for MySteppingAction");
    myFloatCmd->AvailableForStates(G4State_PreInit, G4State_Idle);

    myFileNameCmd = new G4UIcmdWithAString("/myCommand/filename", this);
    myFileNameCmd->SetGuidance("Set the filename");
    // myFileNameCmd->AvailableForStates(G4State_PreInit, G4State_Idle);
}

MyMessenger::~MyMessenger()
{
    delete myFloatCmd;
    delete myDir;
}

void MyMessenger::SetNewValue(G4UIcommand* command, G4String newValue)
{
    if (command == myFloatCmd) {
        // Without unit
        // mySteppingAction->SetZFromMacro(myFloatCmd->GetNewDoubleValue(newValue));


        G4double value;
        G4String unit;
        std::istringstream iss(newValue);
        iss >> value >> unit;

        // Convert the value to a consistent unit (e.g., mm) based on the input unit
        if (unit == "cm") {
            value *= cm;
        } else if (unit == "m") {
            value *= m;
        } // Add more unit options as needed

        // Set the value in MySteppingAction
        mySteppingAction->SetZFromMacro(value);

    }

    if (command==myFileNameCmd){
        myRunAction->SetFileNameSuffix(newValue);
    }

    


}
