# A guide on how does this simulation work

Guide written and edited by [P. Monti-Guarnieri](mailto:monti.guarnieri.p@gmail.com). 

Date of the last update of this guide: 2023/05/08.

## Table Of Contents

[[_TOC_]]

## A general idea

This simulation code is composed essentially of two parts:
1. A standard G4 simulation of the KLEVER 2021 beamtest setup. This aims simply at simulating the experimental setup there employed (with all the corresponding detectors and units) and measuring the quantities relevant for the data analysis. In other words, this is the part which resembles the most an ordinary G4 simulation. This part is implemented in the following files, included in the [`CERN2022/src`](CERN2022/src) folder (and the [`main.cc`](CERN2022/main.cc) file, of course), with the layout described in the next sections (*for clarity: here we mention only the .cc files, but of course also the .hh should be taken in consideration*):
	- [`ActionInitialization.cc`](CERN2022/src/ActionInitialization.cc)
	- [`CustomHit.cc`](CERN2022/src/CustomHit.cc)
	- [`CustomSD.cc`](CERN2022/src/CustomSD.cc)
	- [`DetectorConstruction.cc`](CERN2022/src/DetectorConstruction.cc)
	- [`EventAction.cc`](CERN2022/src/EventAction.cc)
	- [`LogicalCrystalVolume.cc`](CERN2022/src/LogicalCrystalVolume.cc)
	- [`PrimaryGeneratorAction.cc`](CERN2022/src/PrimaryGeneratorAction.cc)
	- [`RunAction.cc`](CERN2022/src/RunAction.cc)

2. A custom modification of the Geant4 PhysicsList, which allows to implement the non-standard phyisics of oriented crystals. This is implemented in the following files, with the layout which is detailed in the next sections:
	- [`BetheHeitlerModel.cc`](BetheHeitlerModel.cc)
	- [`Bremsstrahlungcorrection.cc`](Bremsstrahlungcorrection.cc)
	- [`eBremsstrahlung_electron.cc`](eBremsstrahlung_electron.cc)
	- [`eBremsstrahlung_positron.cc`](eBremsstrahlung_positron.cc)
	- [`eBremsstrahlungRelModel_electron.cc`](eBremsstrahlungRelModel_electron.cc)
	- [`eBremsstrahlungRelModel_positron.cc`](eBremsstrahlungRelModel_positron.cc)
	- [`EmStandardPhysics.cc`](EmStandardPhysics.cc)
	- [`GammaConversionCrystal.cc`](GammaConversionCrystal.cc)
	- [`PairProductioncorrection.cc`](PairProductioncorrection.cc)
	- [`PairProductionRelModel.cc`](PairProductionRelModel.cc)
	- [`PhysicsList.cc`](PhysicsList.cc)
	- [`SeltzerBergerModel_electron.cc`](SeltzerBergerModel_electron.cc)
	- [`SeltzerBergerModel_positron.cc`](SeltzerBergerModel_positron.cc)

Finally, there is a number of files which are reminscenses of older versions of the code and are here only for historical reasons (*which means that I didn't found their use yet, since their only calls in the code seem to be all commented, and that I should remove them for clarity...*). Such files are:
- [`eBremsstrahlungAmorph.cc`](eBremsstrahlungAmorph.cc)
- [`GammaConversion.cc`](GammaConversion.cc)
- [`GammaConversionAmorph.cc`](GammaConversionAmorph.cc)
- [`SeltzerBergerModel.cc`](SeltzerBergerModel.cc)

We shall now describe each part in more detail and underline the trickiest part of the code. 

## Output file structure

Before delving in the actual structure of the code, it is a good idea to present the structure of the files which are produced as output. These files are produced by default in in [`CERN2022/out_data`](CERN2022/out_data), following the structure defined in the constructor of [`RunAction`](CERN2022/src/RunAction.cc#L39) and implemented in [`EventAction::EndofEventAction`](CERN2022/src/EventAction.cc#L17). The files are ROOT trees, which contain several ntuples, with the following structure:

| Name of the ntuple | Content                                                 |
|:------------------:|:-------------------------------------------------------:|
| NEvent             | Event number (ID)                                       |
| Tracker_NHit_X_0   | Number of particles crossing T1 (x)                     |
| Tracker_NHit_Y_0   | Number of particles crossing T1 (y)                     |
| Tracker_NHit_X_1   | Number of particles crossing T2 (x)                     |
| Tracker_NHit_Y_1   | Number of particles crossing T2 (y)                     |
| Tracker_NHit_X_2   | Number of particles crossing C1 (x)                     |
| Tracker_NHit_Y_2   | Number of particles crossing C1 (y)                     |
| Tracker_NHit_X_3   | Number of particles crossing C2 (x)                     |
| Tracker_NHit_Y_3   | Number of particles crossing C2 (y)                     |
| Tracker_X_0        | Hit position on the plane of T1 (x)                     |
| Tracker_Y_0        | Hit position on the plane of T1 (y)                     |
| Tracker_X_1        | Hit position on the plane of T2 (x)                     |
| Tracker_Y_1        | Hit position on the plane of T2 (y)                     |
| Tracker_X_2        | Hit position on the plane of C1 (x)                     |
| Tracker_Y_2        | Hit position on the plane of C1 (y)                     |
| Tracker_X_3        | Hit position on the plane of C2 (x)                     |
| Tracker_Y_3        | Hit position on the plane of C2 (y)                     |
| GammaCal_EDep_00   | Energy deposited in the UL tower of the $\gamma$-CAL    |
| GammaCal_EDep_01   | Energy deposited in the UC tower of the $\gamma$-CAL    |
| GammaCal_EDep_02   | Energy deposited in the UR tower of the $\gamma$-CAL    |
| GammaCal_EDep_10   | Energy deposited in the CL tower of the $\gamma$-CAL    |
| GammaCal_EDep_11   | Energy deposited in the CC tower of the $\gamma$-CAL    |
| GammaCal_EDep_12   | Energy deposited in the CR tower of the $\gamma$-CAL    |
| GammaCal_EDep_20   | Energy deposited in the BL tower of the $\gamma$-CAL    |
| GammaCal_EDep_21   | Energy deposited in the BC tower of the $\gamma$-CAL    |
| GammaCal_EDep_22   | Energy deposited in the BR tower of the $\gamma$-CAL    |
| Crystal_EDep       | Energy deposited in the crystal under test              |
| MC_EDep            | Energy deposited in the Multiplicity Counter            |

Every element of the ntuples is a value measured in a different event. Pay attention to the following conventions:

- The number of particles crossing the $x$ and $y$ plane of a silicon telescope are identical, since they are computed on the same physical volume.
- The number of particles crossing the $x$ and $y$ plane of a silicon chamber are not identical, since they are computed on different physical volumes.
- All the hit positions are measured in cm.
- All the energy deposits are measured in GeV.
- The naming conventions for the $\gamma$-CAL towers is to use a pair of letters: the first describes the row (Upper, Central, Bottom) and the second the column (Left, Center, Right), as viewed by the beam incident on the calorimeter.


# Detailed explanation of the code structure


## The `main()`


As usual, this is the base of the G4 simulation. There is nothing exceptional in the function here implemented: all the usual User Initializations are called; after the simulation is concluded, all the necessary elements are closed or deleted. The only relevant thing to notice here is that **in the `main.cc` file a specific variable is defined: `bChanneling`** ([line 62](CERN2022/main.cc#L59)). This is the variable which defines if the modifications of the EM processes due to the oriented crystals are applied (`bChanneling=true;`) or not (`bChanneling=false;`). This variable is thus the one which is used to perform choose whether the run will be performed in "random orientation" condition or in "alignment" (see below).

## The CMakeList


Here it is defined which files will be considered in the compilation of the simulation (and in what order). In general, th, it could be useful to stress the fact that it is in this file (on [line 6](CERN2022/CMakeLists.txt#L6)) that the name of the Geant4 application is defined (in this case it is "klever21", while in other simulations it could be "exampleB1" and such). Then, line 6 is the **only one allowed for modification**.


## The "standard" part


This is the part where the baseline framework for the G4 simulation is constructed. The following files are used:

- [`ActionInitialization.cc`](CERN2022/src/ActionInitialization.cc). Here, all the standard G4 classes required to run the simulation (e.g., RunAction and EventAction) are conjured. Nothing special.

- [`CustomHit.cc`](CERN2022/src/CustomHit.cc) and [`CustomSD.cc`](CERN2022/src/CustomSD.cc). These files are used to implement the **Sensitive Detectors** (SD) in the simulation. As it is well explained in the reference guides and presentations (see for example [here](https://www.slac.stanford.edu/xorg/geant4/KISTI2019/HandsOn3/) and also [here](https://agenda.infn.it/event/5981/sessions/10089/attachments/43814/52014/SensitiveDetector_alghero2013.pdf)), SDs are just one of the possible ways to implement the measurement of the relevant quantities in the volumes of interest: their presence allows to **not define a custom SteppingAction** class, but rather to initialize these objects at the beginning of the EventAction and collect their output at the end of the EventAction.  
	
	More specifically, it is [`CustomSD.cc`](CERN2022/src/CustomSD.cc) that implements how the SDs actually work. Two types of detectors are here defined: [`VolumeEDepSD`](CERN2022/src/CustomSD.cc#L11) and [`VolumeTrackingSD`](CERN2022/src/CustomSD.cc#L39). The first is used for the construction of any detector which measures the energy deposited in a volume: such are calorimeters, scintillators, crystals coupled to SiPMs and so on. The second is used for the construction of any tracking detector (in our case, both silicon microstrip telescopes and chambers - the difference is just the volume used in the two cases). In general, both types of SD work in the same way: during an event, whenever a particle crosses the volume of a given SD, a "hit" is generated and a number of quantities (user-defined) are stored. At the end of the G4Event, a custom processing may be applied to the recorded hits. In this sense, the hit collection produced in a given event is a vector of arbitrary length: the actual length depends on how many particles crossed the active volume, the length of the tracks and so on.  

	**Important**: [`CustomSD.cc`](CERN2022/src/CustomSD.cc) defines how the SDs work during each call at the SteppingAction level, while [`EventAction.cc`](CERN2022/src/EventAction.cc) defines how the hits are post-processed (and saved in the readout file!).  

	Let us examine now [`CustomSD.cc`](CERN2022/src/CustomSD.cc). We can notice that, as it is customary, each SD comes with two methods, which must be defined in order for all to work well: these are `Initialize` and `ProcessHits`. While the former is just used to pre-initialize the variables which will be filled during the G4SteppingActions, the latter is the one who does the trick: it computes the deposited energy (for the calorimeters) or the deposited energy + the hit positions (for the tracking detectors). In fact, notice the use of the following functions:
	- `SetTrackId`
	- `SetX`
	- `SetEDep`

- [`DetectorConstruction.cc`](CERN2022/src/DetectorConstruction.cc), as usual, describes the physical components of the world. The [`Construct`](CERN2022/src/DetectorConstruction.cc#L34) method builds all the materials which compose the world and defines the Physical and Logical volumes of all the detectors (i.e., trakers, calorimeters, scintillators, etc.) and the passive volumes (such as the bending magnet). The [`ConstructSDandField`](CERN2022/src/DetectorConstruction.cc#L392) method performs two operations:
	1. It constructs the magnetic field which is attached to the corresponding volume.
	2. It instances the Sensitive Detectors. **Note**: there are 9 different SD-calorimeters in the volume where the $\gamma$-CAL is localized, since each BGO tower is readout separately.

- [`EventAction.cc`](CERN2022/src/EventAction.cc), as usual, describes what happens in each event. Notice that here there is no function explicitly defining what happens at the *beginning* of the EventAction: this absence is due to the fact that the only thing happening in such moment in time is the initialization of the SDs, which is explicitly declared in [`CustomSD.cc`](CERN2022/src/CustomSD.cc) (see the `Initialize` methods of the SD classes). Thus, in the EventAction we only need to describe what happens at the *end* of the EventAction. Notice how we use the [`GetHCofThisEvent`](CERN2022/src/EventAction.cc#L27) to get the specific hit collection of this event (line 23) and then we look for each different set of hits. The syntax of the [`GetCollectionID`](CERN2022/src/EventAction.cc#L32) function is somewhat peculiar: to understand it better, check for instance [this](https://www.ge.infn.it/geant4/training/ornl_2008/readoutanddetectorresponse.pdf) presentation at slide 31.
	
	After getting the hit collections, we start saving the outputs. Notice the use of the function `FillNtupleDColumn`: this allows to fill a ROOT-ntuple, which is declared in the [`RunAction.cc`](CERN2022/src/RunAction.cc)  file. **Important note**: the ROOT-ntuples are effectively filled during each event, but they are **written on disk only at the end of the RunAction**. This means that **interrupting the simulation halfway-through leads to the loss of all the data acquired in the given run**. The only way to prevent this effect is to divide the desired statistics in several runs: for instance, if you want to obtain 1M data events, you could perform 10 runs, each one composed of 100k events and corresponding to a file with a different name. Then, the files should be merged, for instance through the [`hadd`](mailto:gsaibene@studenti.uninsubria.it) command. This strategy has been implemented in the [`mastercall.sh`](mastercall.sh) script.

- [`LogicalCrystalVolume.cc`](CERN2022/src/LogicalCrystalVolume.cc) defines a custom class for the creation of the logical volume of the crystal under test. It is not really clear to me *why* a different class was needed, but the code works...

- [`PrimaryGeneratorAction.cc`](CERN2022/src/PrimaryGeneratorAction.cc), which instantiates the particle source. Notice that the actual commands for the definition of the beam energy and profile are given in the macros (e.g., [`run_forphysics.mac`](CERN2022/macros/run_forphysics.mac)).

- [`RunAction.cc`](CERN2022/macros/RunAction.cc), which defines what is done in a given Run. The only relevant thing to mention here is the fact that the RunAction constructor defines the structure of the output file and opens the latter, while the destructor writes the output on disk and closes the file. This is pretty standard when using the `G4AnalysisMessenger` tool for producing the output (which is the standard way to produce ROOT output files).

For clarity, we should also consider the files available in the [`include`](CERN2022/include/) folder. Essentially, here we can find all the `.hh` files which complement the corresponding `.cc` files available in [`src`](CERN2022/src/), since they define the functions, classes and variables used by the latter. For such files we do not need to comment any further, since we have already explained what they do.

### Historical note

In some older versions of this code (see the first commits on this repository) there are two other files in the `./include` folder which do not appear in the `./src`. These are:

- `Analysis.hh`. This files did pretty much nothing: *in the most general way*, it could be used to perform an analysis on the acquired data, but in this simulation such behaviour has not been implemented. However, this file was initially used to perform one task: it made it so that the output file was a ROOT tree. Before Geant4.10.something, this specification was necessary: simply stating that the output file is `somename.root` did not imply a ROOT structure! Thus, it was necessary to add in the header of the script the line:

```cpp
#include <g4root.hh>
```

In this case, such line was added in `Analysis.hh`, which was imported in the [`RunAction.cc`](CERN2022/macros/RunAction.cc) and [`EventAction.cc`](CERN2022/src/EventAction.cc) files. After Geant4.10.something, this problem was fixed: adding the `.root` extension to the file name now implies that the file will be a ROOT one. In fact, in the current release of this code, this file has been removed entirely (since the compilation is adviced to be performed *at least* with Geant4.11.0.2)

- `TestMode.cc` (*a `.cc` file put in the `./include` folder? Someone did not pay attention during catechism*). This was essentially a whole simulation written in a single file. It was used originally to test the physics of oriented crystal with a simple setup and a controlled and very verbose output. It was not really used anywhere, and thus it was removed.


## The PhysicsList modification part


### Introduction: how to modify a PhysicsList

This is the part where the Physics List is actually modified, if the system is defined to be working in "orientation" condition (check the value of the [`bChanneling`](main.cc#L59) variable, defined in [`main.cc`](`main.cc`)). In this case, it has no sense to just list the various files which implement this modification, since they are called in a specific order, so it is better to explain the logic behind it.

The starting point of this discussion is located around line 75 of the [`main`](main.cc#L75), with the following code:
```cpp
G4int PhysicsListVerbosity = 1;
G4VModularPhysicsList* modifiedPhysicsList = new FTFP_BERT(PhysicsListVerbosity);
modifiedPhysicsList->ReplacePhysics(new EmStandardPhysics(PhysicsListVerbosity,nullstr,matID));
runManager->SetUserInitialization(modifiedPhysicsList);
```

This means that the starting point is the **"FTFP_BERT"** PhysicsList. For more details on this List specifically, please see this [good presentation](https://www.slac.stanford.edu/xorg/geant4/SLACTutorial14/Physics1.pdf) around slide 25. Essentially, this is a pre-packaged PhysicsList which includes both electromagnetic and hadronic physical processes. It is the one recommended by the Geant4 development team and it is also probably one of the most common in HEP applications, at least in the cases where it is not required to do something exotic (such as considering Standard Model-forbidden processes, or accurately modelling the interactions and motion of neutrons below 20 MeV - refer to the presentation indicated above for **a very good explanation** of what this list includes and what not, and also what in general is a PhysicsList!). Note that the argument variable "PhysicsListVerbosity" in the above code simply states the verbosity for the call.

After constructing the default FTFP_BERT PhysicsList, we see that the `ReplacePhysics` method is invoked: this allows to replace the EM part of the FTFP_BERT PhysicsList with another list, which is `EmStandardPhysics` and is defined in the [`EmStandardPhysics.cc`](CERN2022/src/EmStandardPhysics.cc) file.  **Note** that this approach should allow to only modify the electromagnetic interactions, while maintaining the hadronic ones, as defined by FTFP_BERT. This fact is confirmed by line 116 of [`EmStandardPhysics.cc`](CERN2022/src/EmStandardPhysics.cc), which states:
```cpp
SetPhysicsType(bElectromagnetic);
```
meaning that `ReplacePhysics` will only replace the electromagnetic part of the Physics List, and not the other components.

`EmStandardPhysics` is a PhysicsList in the true sense: if we inspect its code, we can notice that two fundamental methods are implemented, which always need to be present in a PhysicsList:
- `ConstructParticle`. This is used by Geant4 to know which are the particles that *can* be generated/involved during the modified electromagnetic processes. 
- `ConstructProcess`. This is used by Geant4 to know which process may take place at each Step. **Note**: each considered process must be instantiated together with a "Model", following a syntax such as the following (which we took from the code itself):
```cpp
G4MuMultipleScattering* mumsc = new G4MuMultipleScattering();
mumsc->SetEmModel(new G4WentzelVIModel());
```

Notice here how first the process is instantiated (first line) and then the Model is assigned (second line). There could be different Models that describe the same process: for instance, different Models could be more suited to different energy intervals, or to different spatial scales. Inspecting the whole code, we can notice that all the standard electromagnetic process, along with the corresponding "standard" models, are regularly instantiated. The only "weird" ones are:
1. **Bremsstrahlung** (BS)
2. **Pair production** (PP)

which are instantiated using three custom classes (`GammaConversionCrystal`, `eBremsstrahlung_electron` and `eBremsstrahlung_positron`). However, if we inspect the respective codes, then we notice that they do indeeed implement some standard Geant4 processes. In facts, the Strong Field-enhanced processes are instantiated **later in the Physics List**.

Before giving more detail about how the processes are implemented, it is convenient to explain that the idea behind this code is that the cross-sections of the "standard" bremsstrahlung and pair production are replaced with a set of values, which are computed by interpolating the tabulation values obtain through numerical solutions of the Baier-Katkov equations, describing the motion of a photon, electron or positron in an oriented lattice. Such simulations were performed by V. Tikhomirov and V. Haurylavets and a few other members of the STORM collaboration and require, as input: a lattice structure, a crystal thickness and an incidence angle. Please note that this modification of the cross-section, as it is written, **affects only a specified region**, regardless of the considered materials, particles and incidence angles. Previous versions of this code (see commits before 2023.03.13) implemented a modification which affected the whole world instead.


### On the assumptions of this code


If all that have been said until this point is clear, we can spend a few words to stress a few of the assumptions under which this code was written:

> 1. This code does not consider the channeling phenomenon in any way, nor the emission of channeling radiation. It is thus **effectively usable only whenever the energy of the primary beam is high enough that only the Strong Field effects may be considered**. In other words, this code is **not suitable** to simulate the interactions occurring in a crystal exposed to a beam with an energy lower than $\sim$ 20 GeV.
> 
> 2. **This is not a complete simulation of the Strong Field interactions** which occur in the oriented crystal, since a single set of enhancement coefficients is **applied to all the incidence particles and all the materials in a specified region**, independently from the propagation angles.
>
> 3. **This is not a general instrument** for the simulation of the Strong Field processes in the oriented crystals, since **the only materials and alignments that can be implemented are the ones which were studied numerically** by the members of the collaboration.

Some comments regarding point 2. This choice makes it so that the energy deposit beyond $\sim 6$ X$_0$ is not simulated accurately anymore: it is well known that the electromagnetic shower spreads also laterally and not only longitudinally, which means that after some radiation lengths the intensity of the Strong Field boost should decrease. However, such a possibility is not implemented in this code, since the same coefficients are kept for the entire length of the crystals. This means, in practice, that this code **is suitable for the simulation of the energy loss inside only thin crystals** (e.g., $<2 \text{X}_0$), while for medium-sized crystals the accuracy should be evaluated with experimental results as reference, and for long crystals this is definetly not a good tool. In future, a correction for the angular spread of the e.m. shower should be added.

Some comments regarding point 3. At the moment, only the following crystals can be simulated with this code (for the up-to-date list see again `main.cc`, from [line 62](main.cc#L62) onwards):
- W (tungsten), $\langle 111 \rangle$ axis, in axial alignment or with a 3 mrad incidence angle (taking into account the difference between electron and positron interactions).
- PbWO$_4$, either $\langle 100 \rangle$ or $\langle 100\rangle$ axis, only in axial alignment.
- **Work in progress** (as of 2023/08/05, not yet available): the PbF$_2$ $\langle 100\rangle$ axis, in axial alignment. 

Now it should be clear what it really means when we talk about "simulating the Strong Field processes occurring in the oriented crystals". Hopefully, in a few years the STORM/OREO collaboration should develop a more accurate and general (read: Geant4-compliant) tool for performing such tasks.

### How to modify this code quickly


Before giving some explanation to how the code works, we want to report how the code should be modified, if the only task of the user is to change the material used for the calculation of the cross-sections (pay attention to the fact that also the material in the [`DetectorConstruction.cc`](CERN2022/src/DetectorConstruction.cc) should be modified by the user!). In that case, it is first necessary to edit the [`main.cc`](main.cc) file:
- The `bChanneling` variable defines if the setup is working in "amorphous" or "axial" mode.
- The `matID` variable defines the crystal and orientation considered for the simulation. The table of conversion from matID to material and orientation is reported in the comments of the code.

Then, there is another thing to do. For reasons which will be explained later in this note, the Strong Field-modified processes are not applied everywhere, but only in the G4regions whose names include the string ["SF_Region"](CERN2022/src/EmStandardPhysics.cc#L380). Such regions are always associated to a **logical volume**: thus, every component affected by the modifications should be placed inside the logical volume of the region. This means that, in order to apply the Strong Field-modified processes to any new volume, it is necessary to define the corresponding G4Region and logical volume (for an example, follow the [DetectorConstruction](CERN2022/src/DetectorConstruction.cc#L152) code)

Finally, for a quick modification of the code, just check the [`mastercall.sh`](mastercall.sh) file. It is the only file you should call to perform the simulation: it handles the compilation and call of the simulation, with the desired statistics and mode of operation. All the output files produced in this way will be placed in the [`Outputfiles`](Outputfiles) folder.


### Explanation on how this code works (for photons)


As we were saying above, the central modifications to the PhysicsList happen in the [`EmStandardPhysics.cc`](CERN2022/src/EmStandardPhysics.cc) file. We can see that all the standard process are defined here: we can recognize them as "standard" because the corresponding names start, by convention, with "G4" (e.g., [`G4PhotoElectricEffect`](CERN2022/src/EmStandardPhysics.cc#L201) is the photoeletric effect). Instead, the custom processes never feature "G4" at the beginning of the name. With this convention in mind, we can find that the only non standard processes are defined at the following lines:

```cpp
208:GammaConversionCrystal* GconvCrys = new GammaConversionCrystal("gammaconversion");
233:eBremsstrahlung_electron* eBs_e = new eBremsstrahlung_electron("electron_bs");
261:eBremsstrahlung_positron* eBs_p = new eBremsstrahlung_positron("positron_bs");
```

Here the code is registering custom versions of the bremsstrahlung and pair production processes. However, if we inspect the `.cc` files bearing the same name, we can notice that the models associated to these processes are **the standard ones**. Why do this? The reasons is that, in older versions of this code, custom models were instantiated there. However, in the current release of the code, this has been changed. In fact, in the older releases, the Strong Field-modified processes were applied **to the entire world**, which is undesirable, for a number of reasons - for instance, it prevents the simulation of a setup where it is important to include also non-oriented interactions. Examples of such setups are:

- A beamtest characterization of oriented crystals in Strong Field condition, performed with a tagged photon beam, produced by bremsstrahlung of a primary electron beam (i.e. the Klever 2021 beamtest setup!)
- A study of the performance of an OREO-like calorimeter, composed of both oriented and non oriented crystals.

In order to guarantee that the same physical thresholds are set, the old code was kept but the models were swapped. Instead, at this point, the Strong Field processes are applied only in a list of specified volumes, which is defined by the G4Regions declared in the [`DetectorConstruction`](CERN2022/src/DetectorConstruction.cc) with a name including the **"SF_Region"** string. The way this is done is described very well in [this](https://indico.esa.int/event/82/contributions/3253/attachments/2609/3031/HandsOn-microdosimetry.pdf) presentation and also in the [**microdosimetry**](https://gitlab.cern.ch/geant4/geant4/-/tree/master/examples/extended/medical/dna/microdosimetry) example. In synthesis, the idea is that:
- An ordinary Physics List is instantiated in a user file. This list must include all the **electromagnetic processes** that will exists, both in the "world" and in the "target" region. **Note**: at the time of writing (2023.05.08), this operation can be done only for electromagnetic processes, and not yet for others (such as the hadronic ones).
- Each Process in the world is associated with a Model. If the process must not considered in the world, then a G4DummyModel must be associated to it.
- After the intantiation, a number of Models must be associated to all the given processes but only in the target region, making use of the [`SetExtraEmModel`](https://apc.u-paris.fr/~franco/g4doxy/html/classG4EmConfigurator.html#694273cb824d06c5eda874c9498b6fa0) function. If we inspect the help of this function, we find out that its syntax is:

	```cpp
	void G4EmConfigurator::SetExtraEmModel(
			const G4String & particleName,
			const G4String & processName,
			G4VEmModel *,
			const G4String & regionName = "",
			G4double emin = 0.0,
			G4double emax = DBL_MAX,
			G4VEmFluctuationModel * fm = 0)
	```

	where:
	- `particleName` must respect the ordinary convention for particle naming in Geant4 (see for instance [this](http://fismed.ciemat.es/GAMOS/GAMOS_doc/GAMOS.5.1.0/x11519.html) unofficial guide)
	- `processName` must be the one declared as first argument of the corresponding process (see for instance [here](CERN2022/src/EmStandardPhysics.cc#L403)), or otherwise the default name of the process (which is defined in the `.hh` file of the process, see for instance [here](https://apc.u-paris.fr/~franco/g4doxy/html/G4eplusAnnihilation_8hh-source.html) to know that the default name of the `G4eplusAnnihilation` process, i.e. the positron annihilation, is "annihil")
	- `regionName` is the name of the G4Region of interest (in our case, "SF_Region")
	- `emin` and `emax` are the minimum and maximum energy for the application of the model (see again the presentation described above for more details and caveats)
	- `fm` is the Geant4 Fluctuation Model used for the specific case. This is a model sometimes required, for instance in the case of the ionization process. To know if this is the case for a given process, check the `InitialiseProcess` method of the process and search for the call to the `AddEmModel` function. 

If we inspect our file, we find out that this is exactly what is done! In facts, we can see that the following custom models are instantied and applied to the target region:

```cpp
391:mod = new SeltzerBergerModel_electron(fMatID);
408:mod = new SeltzerBergerModel_positron(fMatID);
426:mod = new eBremsstrahlungRelModel_electron(fMatID);
442:mod = new eBremsstrahlungRelModel_positron(fMatID);
464:mod = new BetheHeitlerModel(fMatID);  
481:mod = new PairProductionRelModel(fMatID);
```

These models allow to describe bremsstrahlung and pair production for both low-energy and high-energy particles (where the energy threshold for the "high energy" processes is $\sim 80$ GeV). Each model is defined in a file with the same name. Then, if we want to understand how the Pair Production process is modified by the Strong Field in the oriented crystals, we should check the [`BetheHeitlerModel`](CERN2022/src/BetheHeitlerModel.cc) file. Note again that, since the name of the model does not include "G4", we can imagine that this is a custom model, and actually it is so. We don't want to delve too deep into the code of this last file, but we want to stress the fact that here is where the actual modifications of the BS process take place: in fact, we can see that the `BetheHeitlerModel::ComputeCrossSectionPerAtom` method is declared! We can imagine that this is one of the few who does the trick. This is not the only one, though. In fact, at line 161 of [`BetheHeitlerModel`](CERN2022/src/BetheHeitlerModel.cc#L161) we see that a call such as the following is invoked:

```cpp
xSection=xSection*correction.ComputeCorrection(gammaEnergy); //ComputeCorrection(gammaEnergy);
```

What is exactly this "correction" entity? By checking the [`BetheHeitlerModel.hh`](CERN2022/include/BetheHeitlerModel.hh#L111) file we can find that this is derived from the `PairProductionCorrection` class. This is, of course, defined in the [`PairProduction.cc`](CERN2022/src/PairProduction.cc) file. If we check this code, we can find immediately that here it happens the switch from material to material. This essentially means that the pair-production cross-section is corrected with the products of two terms: one depends only on the atomic number of the material, while the other (the one given in `PairProductionCorrection`) depends also on the actual structure of the crystalline lattice.

If we go back to the Physics List, we can see that something similar is done with the high energy model, namely `PairProductionRelModel`. In this case, we are invoking a model which is specific for the ultra-relativistic regime (physically, we are considering here the fact that at high enough energies there is a partial modification of the process, also due to the Landau-Pomerachunk-Migdal suppression). The corresponding modification is defined in the [`PairProductionRelModel.cc`](CERN2022/src/PairProductionRelModel.cc)) file, of course. Let's skip completely the file, since we don't want to understand how all the correction works, and let us check instead the corresponding `.hh` file. We can see here that once again there is an instance of the `PairProductionCorrection` class. This means that once again we go back to the same correction model, with a few differences. 

And this is all! Like, really. We have entirely omitted to explain how each file actually implements the modifications of the physical processes, but as an introduction to this code this should be enough. Now, it should be clear what does each file do: the rest is up to the reader.


### Explanation on how this code works (for electrons and positrons)


We have explained in the previous section how does the code work for incident photons. But what about electrons and positrons? Well, the code flow is essentially the same, thus it is not necessary to repeat each step explicitly. Let's consider electrons and positrons: in this case, the models to be checked are `SeltzerBergerModel_electron` and `SeltzerBergerModel_positron`. Both models invoke an instance of the `BremsstrahlungCorrection` class, which is defined in the file with the same name. Once again we find out that the correction to the BS cross-section is composed of two pieces, one of which depends on the crystal structure (and thus requires a switch on the type of target). We can now understand *why* the points where the code should be modified in order to implement a specific type of target are exactly the ones listed some sections ago!

# Summary


At this point, it should be clear what all the pieces of the code do. However, it could still be useful to graphically summarize how all the pieces of the code "talk" to each other. The flow of the photon modification part is something like:

```mermaid
flowchart TD
    A[EmStandardPhysics.cc] --> |RegisterProcess:|C[GammaConversionCrystal]
    C --> |SetEmModel:|D[G4BetheHeitlerModel]    
    C --> |SetEmModel:|E[G4PairProductionRelModel]    
    A --> |SetExtraEmModel:|H[BetheHeitlerModel]
    A --> |SetExtraEmModel:|I[PairProductionRelModel]
    H --> J[PairProductioncorrection]
    I --> J
```

The flow for electrons is:

```mermaid
flowchart TD
    A[EmStandardPhysics.cc] --> |RegisterProcess:|C[eBremsstrahlung_electron]
    C --> |SetEmModel:|D[G4SeltzerBergerModel]    
    C --> |SetEmModel:|E[G4eBremsstrahlungRelModel]    
    A --> |SetExtraEmModel:|H[SeltzerBergerModel_electron]
    A --> |SetExtraEmModel:|I[eBremsstrahlungRelModel_electron]
    H --> J[Bremsstrahlungcorrection]
    I --> J
```

Finally, the flow for positrons is:

```mermaid
flowchart TD
    A[EmStandardPhysics.cc] --> |RegisterProcess:|C[eBremsstrahlung_positron]
    C --> |SetEmModel:|D[G4SeltzerBergerModel]    
    C --> |SetEmModel:|E[G4eBremsstrahlungRelModel]    
    A --> |SetExtraEmModel:|H[SeltzerBergerModel_positron]
    A --> |SetExtraEmModel:|I[eBremsstrahlungRelModel_positron]
    H --> J[Bremsstrahlungcorrection]
    I --> J
```

# Appendix

List of a few of the other useful resources:
- Other examples where a custom Physics List is used: [microelectronics](https://gitlab.cern.ch/geant4/geant4/-/tree/master/examples/advanced/microelectronics/src), [dnaphysics](https://gitlab.cern.ch/geant4/geant4/-/tree/master/examples/extended/medical/dna/dnaphysics) and [dnadamage1](https://gitlab.cern.ch/geant4/geant4/-/tree/master/examples/extended/medical/dna/dnadamage1/src).
- Documentation of the [G4EmConfigurator](https://apc.u-paris.fr/~franco/g4doxy/html/classG4EmConfigurator.html) class
- [Changelog](https://indico.cern.ch/event/773049/contributions/3474766/attachments/1937617/3211571/G4EmCHEP2019_v6.pdf) where the possibility to use localized EM Physics Lists was introduced (an implicit reference to this point is given [here](https://indico.cern.ch/event/781244/contributions/3251929/attachments/1783382/2902583/EM.pdf)).
- A guide on how to write [custom Physics Lists](https://geant4-userdoc.web.cern.ch/UsersGuides/PhysicsListGuide/html/physicslistguide.html).