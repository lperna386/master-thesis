#!/bin/bash

buildDir="CERN2022-build"

# Enter the build dir if exists
[ ! -d $buildDir  ] && echo "Build dir missing. Are you in the correct place?" && exit 1
cd $buildDir 


exeFile="klever21"

# Macro file and dir
macroDir="macros"
macroFileName="run_forphysics.mac"
macroFile=${macroDir}/${macroFileName}

# Output dirs
tmpOutData="out_data"
finalOutData="../Outputfiles"
mkdir -p $finalOutData

# Some preliminary controls
[ ! -f $exeFile  ] && echo "Exe file missing. Did you forget to compile?" && exit 1
[ ! -f $macroFile  ] && echo "Macro file missing. Did you forget to cmake?" && exit 1


# Intervallo di energie su cui iterare
for i in $(seq 10 10 120); do
    echo $i # Use only this line in the loop to ckeck which numbers are used

    # Replace energy
    # I look for a line which contains
    # /gps/ene/mono<SPACE>ddd<SPACE>GeV 
    # being ddd an arbitrary number of digits
    sed -i "s|\(/gps/ene/mono \)[0-9]\+\( GeV\)|\1$i\2|" $macroFile

    # Call the simulation
    ./$exeFile $macroFile

    # At the end, move the file
    # SET HERE THE CORRECT FILE NAME, SUCH AS tbeamdata0000.root
    mv -v ${tmpOutData}/tbeamdata0000.root "${finalOutData}/out_${i}_GeV.root"


done