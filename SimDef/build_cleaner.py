# ==========================================================================================
#                                          HEADER
# ==========================================================================================
# Lo scopo di questo script è svuotare la cartella di build della simulazione, con eccezione
# degli eventuali .gitkeep e .gitignore
#
# Call (supponendo di essere già nella cartella di build) :
# $ python3 ../build_cleaner.py
#
# ==========================================================================================
#                                  PREAMBOLO (load moduli)
# ==========================================================================================
# Import dei moduli esterni
import os                           # Per agire sul sistema operativo
import sys                          # [come sopra]
import glob                         # Per manipolazione dei path
import shutil                       # Per rimuovere i tree

# ==========================================================================================
#                                         SCRIPT
# ==========================================================================================
# Estraggo l'elenco dei file presenti nella cartella di build
thisfolder = os.path.dirname(os.path.abspath(__file__))
locfolder = thisfolder + "/CERN2022-build/"
allfiles = sorted(glob.glob(locfolder + '*'))

# Definisco le cartelle da non rimuovere
keep = [".gitkeep",".gitignore"]

# Rimuovo tutte le cartelle e i file richiesti
print(f"build_cleaner.py is currently located in the folder: {locfolder}")
print(f"The following files are being removed from the current location: \n")
for el in allfiles:
    # Estraggo il nome del file i-esimo (path-stripped)
    strname = el.split('/')[-1]

    # Se il file non è nella lista di quelli da tenere, lo rimuovo
    if strname not in keep:
        print(strname)
        try:
            os.remove(el)
        except IsADirectoryError:
            shutil.rmtree(el)


# ==========================================================================================
#                                         SCRIPT PARTE 2
# ==========================================================================================
# Estraggo l'elenco dei file presenti nella cartella di outputfiles
locfolder = thisfolder + "/Outputfiles/"
allfiles = sorted(glob.glob(locfolder + '*'))

# Definisco le cartelle e file da non rimuovere
keep = [".gitkeep",".gitignore","tbeamdata_amorphous.root","tbeamdata_axial.root"]

# Rimuovo tutte le cartelle e i file richiesti
print(f"")
print(f"build_cleaner.py is currently located in the folder: {locfolder}")
print(f"The following files are being removed from the current location: \n")
for el in allfiles:
    # Estraggo il nome del file i-esimo (path-stripped)
    strname = el.split('/')[-1]

    # Se il file non è nella lista di quelli da tenere, lo rimuovo
    if strname not in keep:
        print(strname)
        try:
            os.remove(el)
        except IsADirectoryError:
            shutil.rmtree(el)

print(f"")
print(f"Task completed! \n")