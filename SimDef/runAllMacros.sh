#!/bin/bash

# ISTRUZIONi
# 1. Entra nella cartella build
# 2. Cmake && make
# 3. Torna indietro
# 4. Chiama questo script DALLA CARTELLA IN CUI SI TROVA

buildDir="CERN2022-build"

# Enter the build dir if exists
[ ! -d $buildDir  ] && echo "Build dir missing. Are you in the correct place?" && exit 1
cd $buildDir 


exeFile="klever21"

# Macro file and dir
macroDir="macros"
# extensionOfMacro=".mac"

# Output dirs
tmpOutData="out_data"
finalOutData="../Outputfiles" # Relative to build directory, where I am
mkdir -p $finalOutData

# Some preliminary controls
[ ! -d $macroDir  ] && echo "Macro directory missing. Did you forget to cmake?" && exit 1
[ ! -f $exeFile  ] && echo "Exe file missing. Did you forget to compile?" && exit 1

for f in $(ls $macroDir); do
    echo $f
    # fileNoExtension=$(basename $f $extensionOfMacro)
    fileNoExtension=${f%%.*}
    echo $fileNoExtension

    # Call the simulation
    ./$exeFile $macroDir/$f

    # At the end, move the file
    # SET HERE THE CORRECT FILE NAME, SUCH AS tbeamdata0000.root
    mv -v ${tmpOutData}/tbeamdata0000.root "${finalOutData}/$fileNoExtension.root"

done