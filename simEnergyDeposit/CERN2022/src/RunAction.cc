#include <G4SystemOfUnits.hh>
#include <G4String.hh>

#include "RunAction.hh"
#include "PrimaryGeneratorAction.hh"
#include "DetectorConstruction.hh"

#include "G4AnalysisManager.hh"
#include "G4RunManager.hh"
#include "G4Run.hh"
#include "G4AccumulableManager.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

using namespace std;

// RunAction, class created at run start, with RunAction::EndOfRunAction executed at the end of the run

RunAction::RunAction() :  G4UserRunAction()
{
    // Load the analysis manager for data output
    G4AnalysisManager* analysis = G4AnalysisManager::Instance();
    analysis->SetVerboseLevel(1);  // set analysis manager verbosity here
  
    // Set progressive printing every X events
	G4int NumofPrintProgress = 250;
    G4RunManager::GetRunManager()->SetPrintProgress(NumofPrintProgress);
    
    // Create output ntuple
    analysis->SetNtupleMerging(true); // Note: merging ntuples is available only with Root output
    analysis->SetFirstNtupleId(0);
    analysis->CreateNtuple("outData", "output data");

    // Create the ntuple columns. Note: the hit positions are measured in cm
    // and the energy deposits are measured in GeV.
	
    analysis->CreateNtupleDColumn("NEvent");            // 0
    analysis->CreateNtupleDColumn("Tracker_NHit_X_0");  // 1
    analysis->CreateNtupleDColumn("Tracker_NHit_Y_0");  // 2 (identical to 1)
    analysis->CreateNtupleDColumn("Tracker_NHit_X_1");  // 3
    analysis->CreateNtupleDColumn("Tracker_NHit_Y_1");  // 4 (identical to 3)
    analysis->CreateNtupleDColumn("Tracker_X_0");       // 5
    analysis->CreateNtupleDColumn("Tracker_Y_0");       // 6
    analysis->CreateNtupleDColumn("Tracker_X_1");       // 7
    analysis->CreateNtupleDColumn("Tracker_Y_1");       // 8
   
    analysis->CreateNtupleDColumn("GammaCal_EDep_CC");  // 9; in GeV

    // leo calo TopLeft
    analysis->CreateNtupleDColumn("GammaCal_EDep_TL");  // 10; in GeV
    //leo calo TopRight
    analysis->CreateNtupleDColumn("GammaCal_EDep_TR");  // 11; in GeV
    //leo calo BottomLeft
    analysis->CreateNtupleDColumn("GammaCal_EDep_BL");  // 12; in GeV
    //leo calo BottomRight
    analysis->CreateNtupleDColumn("GammaCal_EDep_BR");  // 13; in GeV
    //leo calo CenterLeft
    analysis->CreateNtupleDColumn("GammaCal_EDep_CL");  // 14; in GeV
    //leo calo CenterRight
    analysis->CreateNtupleDColumn("GammaCal_EDep_CR");  // 15; in GeV
    
    analysis->CreateNtupleDColumn("CrystalA_EDep");   //16
    analysis->CreateNtupleDColumn("CrystaB_EDep");   // 17
    analysis->CreateNtupleDColumn("CrystalC_EDep");   // 18
    analysis->FinishNtuple(0);



    // Create Ntuple for xy coord
    
    analysis->CreateNtuple("EnergyDeposit", "EnergyInfo");

    // leo tolgo questa parte 
    //analysis->CreateNtupleIColumn("EvID");  
    //analysis->CreateNtupleDColumn("Xcrossing");  
    //analysis->CreateNtupleDColumn("Ycrossing");  
    //analysis->CreateNtupleIColumn("PDGEncoding");  
    //analysis->CreateNtupleIColumn("ParticleID");  
    //analysis->CreateNtupleDColumn("totEnergy");  

    //leo creo colonne Ntupla


    analysis->CreateNtupleIColumn("EvID");   
    analysis->CreateNtupleDColumn("totEnergy");
    analysis->CreateNtupleIColumn("PDGEncoding");   
    analysis->CreateNtupleIColumn("ParticleID"); 

    analysis->FinishNtuple(1);

/* leo commento questa parte 

    G4double extHisto = 5;
    analysis->CreateH2("crossingPos", "raw crossing position", 
                        300, -extHisto, extHisto,
                        300, -extHisto, extHisto);
    analysis->CreateH2("crossingWeigh", "weighted crossing position", 
                        300, -extHisto, extHisto,
                        300, -extHisto, extHisto);

*/
}


RunAction::~RunAction()
{}

void RunAction::BeginOfRunAction(const G4Run* run)
{
    // Create the analysis manager.
    auto analysis = G4AnalysisManager::Instance();

    // Define the name of the run file with a fixed format: 4 numbers
    // (e.g., 0001, 0002, 0010, etc). The number to be set is simply the run ID.

    G4int RunNumber = G4RunManager::GetRunManager()->GetCurrentRun()->GetRunID();

    std::ostringstream ss;
    ss << std::setw(4) << std::setfill('0') << RunNumber;
    std::string RunString = ss.str();
    G4String fileName = "./out_data/tbeamdata" + RunString + ".root";

    // Open the output file of the given run
    analysis->OpenFile(fileName);

    // Give a printout of the run which is beginning
    if (IsMaster()) {
      G4cout
       << G4endl
       << "--------------------Begin of Global Run-----------------------";
    }
    else {
        //G4int ithread = G4Threading::G4GetThreadId();

        G4cout
         << G4endl
         << "--------------------Begin of Local Run------------------------"
         << G4endl;
    }
}

void RunAction::EndOfRunAction(const G4Run* run)
{
    // Retrieve the number of events produced in the run
    G4int nofEvents = run->GetNumberOfEvent();
    if (nofEvents == 0) return;

    if (IsMaster())
    {
        G4cout << "-----" << G4endl;
        G4cout << "| RunAction.cc: end of run --> generated events: " << nofEvents << G4endl;
        G4cout << "-----" << G4endl;
    }

    // Write output file & close it
    G4AnalysisManager* analysis = G4AnalysisManager::Instance();
    analysis->Write();
    analysis->CloseFile();
}
